// play video
function play() {
    const audio = document.getElementById("videoSelf");
    const video = document.getElementById('video')
    $(video).find('img').hide()
    audio.play();
}


$(function() {

    const menu = $('#menu').hide().css({'transform' : 'translateY(-15vh)'})
    const captionTitle = $('#captionTitle').hide()
    const captionText = $('#captionText').hide()
    const captionLink = $('#captionLink').hide()
    const captionVertical = $('#captionVertical').hide()

    const animDuration = 600
    const basicDelay = 1000

    $(menu).show('slow', function() {
        $(menu).css({
            'transform' : 'translateY(0vh)', 
            'transition' : '.2s ease-in'
        })
    })
    $(captionTitle).delay( basicDelay ).slideUp(animDuration).show( animDuration );
    $(captionText).delay( basicDelay+200 ).slideUp(animDuration).show( animDuration )
    $(captionLink).delay( basicDelay + 600 ).fadeIn(animDuration)
    $(captionVertical).delay(basicDelay+1000).fadeIn(animDuration, function() {
        $(this).css({'left' : '75%'})
    })
})


// Trigger elements on scroll 
gsap.registerPlugin(ScrollTrigger);


// trigger les elements de la banniere
const tlLive = gsap.timeline({
    scrollTrigger: {
            trigger: '#blockLieu',                 
            toggleActions: "restart none none pause", 
    },
})
tlLive.fromTo('#lieuTitle', .4, {opacity: 0, y:50}, {opacity:1,  y:0, duration:.2, ease: ' "circ.out"'})
tlLive.fromTo('#lieuSep', .4, {opacity: 0, y:50}, {opacity:1,  y:0, duration:.2, ease: ' "circ.out"'})
tlLive.fromTo('#lieuContent', .4, {opacity: 0, y:50}, {opacity:1, y:0, duration:.2, ease: ' "circ.out"'})
tlLive.fromTo('#lieuCTA', .4, {opacity: 0, y:50}, {opacity:1,  y:0, duration:.2, ease: ' "circ.out"'})

// trigger les div lieu et service
const tlserviceTop = gsap.timeline({
    scrollTrigger: {
            trigger: '#blockService',                 
            toggleActions: "restart none none none", 
    },
})
tlserviceTop.fromTo('#homeCarousel', .6, {opacity: 0,  x:-450}, {opacity:1, x:0, duration:.2, ease: ' "circ.out"'})
tlserviceTop.fromTo('#right', .6, {opacity: 0, x:250}, {opacity:1, x:0, duration:.2, ease: ' "circ.out"'})




const tlService = gsap.timeline({
    scrollTrigger: {
            trigger: '#service',                 
            toggleActions: "restart none none none", 
    },
})
tlService.fromTo('#serviceTitle', .6, {opacity: 0, y:50}, {opacity:1,  y:0, duration:.2, ease: ' "circ.out"'})
tlService.fromTo('#serviceHr', .6, {opacity: 0, y:50}, {opacity:1,  y:0, duration:.2, ease: ' "circ.out"'})
tlService.fromTo('#servicePara', .6, {opacity: 0, y:50}, {opacity:1, y:0, duration:.2, ease: ' "circ.out"'})
tlService.fromTo('#serviceBtn', .6, {opacity: 0, y:50}, {opacity:1,  y:0, duration:.2, ease: ' "circ.out"'})

const tlVideo= gsap.timeline({
    scrollTrigger: {
            trigger: '#homeVideoContainer',                 
            toggleActions: "restart none none none", 
    },
})
tlService.fromTo('#video', .2, {opacity: 0}, {opacity:1, duration:.4, ease: 'ease-out'})


// slide in des offres

// $(window).on('resize', function (e) {
    var width = $(window).width();

    if (width > 1400){
        gsap.to('#blockOffer', {
            scrollTrigger: {
                trigger: '#blockOffer',
                start: 'top center', 
                toggleActions: 'restart pause resume none'
            },
            x: -800,
            duration: 3,
        })
    }
// });





